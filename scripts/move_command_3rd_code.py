#!/usr/bin/env python3

import rospy
import math
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry 

max_lin_vel = 0.22
max_ang_vel = 2.84

avg_speed = 0
speed_sum = 0
collisions = 0
counter = 0
is_collided = False
#Manoeuvres
turn_right = False
turn_left = False
turn_around = False
go_straight = False
maze_completed = False
#Odom readings
roll = pitch = yaw = target_rad = 0.0
curr_linvel = curr_angvel = 0




def callback(msg):
    global collisions, is_collided, max_ang_vel, max_lin_vel, counter, turn_right, turn_left, target_rad, turn_around, go_straight, maze_completed
    #Full speed and go straight
    vel.linear.x = max_lin_vel
    vel.angular.z = 0

    #Angles
    middle = 0
    f_right = 300
    f_left = 60
    left = 90
    right = 270
    
    #Reading distances
    middle_dist = msg.ranges[middle]
    f_right_dist = msg.ranges[f_right]
    f_left_dist = msg.ranges[f_left]
    right_dist = msg.ranges[right]
    left_dist = msg.ranges[left]

    #Collision detection
    collision_dist = 0.14

    if not is_collided:
        if middle_dist < collision_dist or f_right_dist < collision_dist or f_left_dist < collision_dist or right_dist < collision_dist or left_dist < collision_dist:
            is_collided = True
            collisions = collisions + 1

    if is_collided:
        if not (middle_dist < collision_dist or f_right_dist < collision_dist or f_left_dist < collision_dist or right_dist < collision_dist or left_dist < collision_dist):
            is_collided = False
        #Do something
    
    turn_speed = 1

    #Turn right
    if turn_right:
        vel.angular.z = -turn_speed
        if abs(target_rad-yaw) < 0.4:
            turn_right = False
            print("stop turning right")
        print_and_pub()
        return
    
    #Turn left
    if turn_left:
        vel.angular.z = turn_speed
        if abs(target_rad-yaw) < 0.4:
            turn_left = False
            print("stop turning left")
        print_and_pub()
        return
    
    #Turn around
    if turn_around:
        vel.linear.x = 0
        vel.angular.z = -turn_speed
        if abs(target_rad-yaw) < 0.4:
            turn_around = False
            print("stop turning around")
        print_and_pub()
        return
    
    #Go straight
    if go_straight:
        if left_dist < 0.8 and f_left_dist < 0.8:
            go_straight = False
            print("stop going straight")
        print_and_pub()
        return

    

    #Difference between 60 and -60 degree distance
    dist_diff = f_right_dist - f_left_dist

    #Turn
    if abs(dist_diff) > 0.4:
        if msg.ranges[f_right + 5] > 0.5:
            target_rad = -90*math.pi/180 + yaw
            if target_rad < -math.pi:
                target_rad = math.pi - (abs(target_rad)%math.pi)
            
            turn_right = True
            print("turn right")
        elif middle_dist < 0.7:
            target_rad = 90*math.pi/180 + yaw
            if target_rad > math.pi:
                target_rad = -math.pi + target_rad%math.pi

            turn_left = True
            print("turn left")
        else:
            #Go straight
            print("go straight")
            go_straight = True

        print_and_pub()
        return
    
    #keep in middle
    vel.angular.z = map_range(dist_diff, -0.1, 0.1, 0.2, -0.2)

    #Avoid head-on collision
    if middle_dist < 0.4:
        turn_around = True
        print("turn around")
        target_rad = -160*math.pi/180 + yaw
        if target_rad < -math.pi:
            target_rad = math.pi - (abs(target_rad)%math.pi)

    print_and_pub()
    
def limit_vel():
    global max_ang_vel, max_lin_vel
    if vel.angular.z > max_ang_vel:
        print(vel.angular.z)
        vel.angular.z = max_ang_vel
    if vel.angular.z < -max_ang_vel:
        print(vel.angular.z)
        vel.angular.z = -max_ang_vel
    if vel.linear.x > max_lin_vel:
        vel.linear.x = max_lin_vel

def print_and_pub():
    global avg_speed, speed_sum, collisions, counter, is_collided, curr_angvel, curr_linvel
    counter += 1
    speed_sum += curr_linvel
    avg_speed = speed_sum / counter
    if counter % 10 == 0:
        print("ang", "{:.2f}".format(curr_angvel), "lin","{:.5f}".format(curr_linvel), "col", collisions, "avg_vel", "{:.5f}".format(avg_speed))
    limit_vel()
    pub.publish(vel)
    
def get_odom(msg):
    global roll, pitch, yaw, curr_linvel, curr_angvel
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
    curr_linvel = msg.twist.twist.linear.x
    curr_angvel = msg.twist.twist.angular.z

def map_range(value, start1, stop1, start2, stop2):
    return (value - start1) / (stop1 - start1) * (stop2 - start2) + start2

def shutdown():
    vel.linear.x = 0
    vel.angular.z = 0
    pub.publish(Twist())
    rospy.sleep(1)

rospy.on_shutdown(shutdown)

rospy.init_node('move_command')
rate = rospy.Rate(10) # 10hz
sub = rospy.Subscriber('scan', LaserScan, callback)
sub2 = rospy.Subscriber('/odom', Odometry, get_odom)
pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
vel = Twist()


try:
    while not rospy.is_shutdown():


        rospy.spin()
except rospy.ROSInterruptException:
    print("Closing Node")


    
