#!/usr/bin/env python3
import rospy
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry 

#\\ Global varibales //#
max_lin_vel = 0.3
turn_speed = 0.9
#Data table values
curr_action = "Following right wall"
avg_speed = speed_sum = collisions = counter = reset = 0
#Manoeuvres
is_collided = False
turn_left = False
turn_around = False
#Odom readings
roll = pitch = yaw = target_rad = 0.0
curr_linvel = curr_angvel = 0
wall_dist_measured = False
dist = 0
#\\

#Control loop
def callback(msg):
    global dist, collisions, is_collided, max_lin_vel, counter, \
           turn_left, target_rad, turn_around, reset, curr_action, \
           wall_dist_measured, turn_speed, curr_angvel
    #Full speed and follow right wall
    vel.linear.x = max_lin_vel
    vel.angular.z = 0
    curr_action = "Following right wall"

    #Reading distances
    middle_dist = msg.ranges[0]
    f_right_dist = msg.ranges[300]
    f_left_dist = msg.ranges[60]
    right_dist = msg.ranges[270]
    left_dist = msg.ranges[90]
    back_dist = msg.ranges[180]
    b_right_dist = msg.ranges[240]
    b_left_dist = msg.ranges[120]

    #Measures the middle of the first coridor.
    if wall_dist_measured==False:
        dist=(f_left_dist + f_right_dist)/2
        print(dist, f_left_dist, f_right_dist)
        wall_dist_measured = True

    #Measure and sums front left angles
    f_left_section = 0
    for i in range(0, 90):
        f_left_section += msg.ranges[i]
    
    #Measure and sums front right angles
    f_right_section = 0
    for i in range(270, 360):
        f_right_section += msg.ranges[i]

    #Collision detection
    if not is_collided:
        collision_dist = 0.13
        if (middle_dist < collision_dist or f_right_dist < collision_dist or 
            f_left_dist < collision_dist or right_dist < collision_dist or 
              left_dist < collision_dist or back_dist < collision_dist or 
           b_right_dist < collision_dist or b_left_dist < collision_dist):
            is_collided = True
            collisions = collisions + 1

    if is_collided:
        curr_action = "Collided"

        #Colision escape dist
        collision_dist = 0.2
        if not(middle_dist < collision_dist or f_right_dist < collision_dist or 
               f_left_dist < collision_dist or right_dist < collision_dist or 
                 left_dist < collision_dist or back_dist < collision_dist or 
              b_right_dist < collision_dist or b_left_dist < collision_dist):
            is_collided = False
            return
        
        #Measure and sums back right angles
        b_left_section = 0
        for i in range(90, 180):
            b_left_section += msg.ranges[i]

        #Measure and sums back right angles
        b_right_section = 0
        for i in range(180, 270):
            b_right_section += msg.ranges[i]

        #lin vel
        if f_right_section + f_left_section > b_right_section + b_left_section:
            vel.linear.x = 0.1
        else:
            vel.linear.x = -0.1

        #ang vel
        if f_right_section + b_right_section > f_left_section + b_left_section:
            vel.angular.z = -0.6
        else:
            vel.angular.z = 0.6
        print_and_pub()
        return
    
    #keep to the right
    error = f_right_dist - dist
    error = min(max(error, -0.09), 0.09)
    vel.angular.z = map_range(error, -0.09, 0.09, turn_speed, -turn_speed)

    #straight speed reduction
    if middle_dist < 1:
        vel.linear.x = map_range(middle_dist, 0.4, 1.0, 0.17, max_lin_vel)

    #Cases
    if turn_around:
        curr_action = "Turning around"
        vel.linear.x = 0
        vel.angular.z = -turn_speed
        if abs(target_rad - yaw) < 0.5:
            turn_around = False
        print_and_pub()
        return
    
    if turn_left:
        curr_action = "Turning left"
        vel.angular.z = turn_speed
        if abs(target_rad - yaw) < 0.4:
            turn_left = False
        print_and_pub()
        return

    #Turn left
    if (f_right_section * 1.2) < f_left_section and \
        middle_dist < (2*dist) and abs(curr_angvel) < 0.15:
        turn_left = True
        #Sets target orientation to current + 90 degrees
        target_rad = 90*math.pi/180 + yaw
        if target_rad > math.pi:
            target_rad = -math.pi + target_rad%math.pi
        
        print_and_pub()
        return

    #Turn around
    if middle_dist < (dist * 1.2) and abs(curr_angvel) < 0.15:
        turn_around = True
        vel.linear.x = 0
        #Sets target orientation to current - 170 degrees
        target_rad = -170*math.pi/180 + yaw
        if target_rad < -math.pi:
            target_rad = math.pi - (abs(target_rad)%math.pi)

    #If no other condition is met publish full speed while folowing wall
    print_and_pub()

#Print data table and publish variables to the subscriber(Turtle Bot)
def print_and_pub():
    global avg_speed, speed_sum, collisions, \
           counter, is_collided, curr_linvel, \
           curr_action, turn_speed
    counter += 1
    speed_sum += curr_linvel
    avg_speed = speed_sum / counter
    #limit turn speed
    vel.angular.z = min(max(vel.angular.z, -turn_speed), turn_speed)

    #Print value table
    print( "Lin Vel","{:.5f}".format(curr_linvel), 
           "| Avg Lin Vel", "{:.5f}".format(avg_speed), 
           "| Collision", collisions, "|",curr_action)
    pub.publish(vel)

#Finds and returns the orientation values from the odemetry topic.
def get_odom(msg):
    global roll, pitch, yaw, curr_linvel, curr_angvel
    orientation_q = msg.pose.pose.orientation
    orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
    (roll, pitch, yaw) = euler_from_quaternion (orientation_list)
    curr_linvel = msg.twist.twist.linear.x
    curr_angvel = msg.twist.twist.angular.z

#Maping function
def map_range(value, start1, stop1, start2, stop2):
    return (value - start1) / (stop1 - start1) * (stop2 - start2) + start2

#Shutdown function with shutdown conditions.
def shutdown():
    vel.linear.x = 0
    vel.angular.z = 0
    print_and_pub()
    
rospy.on_shutdown(shutdown)
rospy.init_node('move_command')
rate = rospy.Rate(30)
sub = rospy.Subscriber('scan', LaserScan, callback)
sub2 = rospy.Subscriber('/odom', Odometry, get_odom)
pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
vel = Twist()


try:
    while not rospy.is_shutdown():
        rospy.spin()
except rospy.ROSInterruptException:
    print("Closing Node")


    
