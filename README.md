# TB3 - Maze solver, group 12

## Introduction
This project is a pakage for ROS Noetic than can be used to make a TB3 Burger solve mazes of different sizes. The program uses a tactic where the robot will follow the right wall to complete the maze it is placed in.
The maze requirements for the project given is a 4m by 4m maze, but as long as the width of the corridors of a maze are the same size, then this project should be able to solve them. 
<br>
Link to reposotory: https://gitlab.com/kevinschoenberg/tb3-bane

## Requirements
This project is written on Ubuntu 20.04 LTS Desktop. You will also need to have Gazebo 11 and ROS Noetic. <br>
The required packages for ROS are: <br>
```
sudo apt install ros-noetic-dynamixel-sdk
sudo apt install ros-noetic-turtlebot3-msgs
sudo apt install ros-noetic-turtlebot3
sudo apt-get install ros-noetic-gazebo-ros-pkgs ros-noetic-gazebo-ros-control
```
Also this rospackage is needed to get the models.
```
cd ~/catkin_ws/src/
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
cd ~/catkin_ws && catkin_make
```


## Installation
To run this project, more specificaly the node to complete the maze. You need to place this repository in ```~/catkin_ws/src/``` and remember to run ```cd ~/catkin_ws && catkin_make```

## Configuration
The to make the robot perform optimally the robot has to be placed inside the maze and close to the middle of the corridor. No further configuration is needed.

To run the maze and the node. You should run the following in two seperate terminals. 
```
export TURTLEBOT3_MODEL=burger
roslaunch tb3-bane maze_4x4.launch
``` 
```
export TURTLEBOT3_MODEL=burger
roslaunch tb3-bane move_node_final.launch
``` 

## Permissions
This project and all its content can be copied and reused without further permission. 